## Benutzung

### Tasks

Folgende Tasks stehen für die Entwicklung zur Verfügung.

`gulp build:development` (synonym zu `gulp`) baut alle nötigen Assets zusammen und führt Code-Style-Checks durch. Es wird nicht minifiziert.

`gulp build:production` Alle Assets werden zusammengebaut, minifiziert und optimiert.

`gulp watch` führt zunächst `gulp` aus, überwacht alle Frontend-Dateien und startet jeweilis die richtigen Tasks, je nachdem welche Dateitypen sich verändert haben.

`gulp serve` identisch zum Watch-Task, allerdings wird zusätzlich Browsersync aktiviert, so dass sich der Browser bei Änderungen automatisch aktualisiert. Mit `gulp serve --no-open` kann verhindert werden, dass sich automatisch das Browserfenster öffnet.

### Sass-Sourcemaps

Sourcemaps sind per default deaktiviert (verlängern die Build-Dauer). Bei Bedarf können sie mit der `--sourcemaps`-Option aktiviert werden:

```
$ gulp serve --sourcemaps
$ gulp serve --no-open --sourcemaps
$ gulp watch --sourcemaps
$ gulp sass:development --sourcemaps
```

### Modernizr

Modernizr ist in den Build-Workflow integriert. Abhängig davon welche Modernizr-Tests (im Sass oder JavaScript) genutzt werden, wird eine `dist/js/modernizr.js`-Datei erzeugt und in die `dist/js/predom.js` integriert.

Wird also bspw. im Sass der Selektor `.no-touchevents {}` oder im JavaScript `if (Modernizr.touchevents) {}` verwendet, muss einmalig `gulp` ausgeführt werden, damit der Modernizr-Task nach Vorkommnissen suchen kann. Danach ist der `touchevents`-Test dann in der `modernizr.js` bzw. `predom.js` integriert und kann beliebig oft genutzt werden.