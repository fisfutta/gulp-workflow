/**
 * Global Configuration for Gulp Tasks
 */
module.exports = {
  autoprefixer: {
    browsers: [
      'last 4 versions',
      'ie 9',
      'android 2.3',
      'android 4',
      'opera 12'
    ],
    cascade: false
  },
  browsersync: {
    options: {
      proxy: "NOTE: needs to be set via gulpconfig.local.js within the project",
      notify: false,
      open: 'local'
    }
  },
  bust: {
    options: {
      length: 10,
      fileName: 'cache-busting.json'
    },
    dest: 'dist'
  },
  clean: {
    path: 'dist'
  },
  copy: {
    js: {
      libs: {
        src: 'assets/js/libs/*.js',
        dest: 'dist/js/libs'
      }
    },
    fonts: {
      src: 'assets/fonts/**/*',
      dest: 'dist/fonts'
    },
    webfonts: {
      src: 'assets/webfonts/**/*',
      dest: 'dist/webfonts'
    }
  },
  css: {
    dest: 'dist/css'
  },
  imagemin: {
    src: 'assets/images/**/*',
    options: {
      progressive: true,
      interlaced: true,
      svgoPlugins: [
        {removeUnknownsAndDefaults: false},
        {cleanupIDs: false}]
    },
    dest: 'dist/images'
  },
  jscs: {
    options: {
      configPath: 'node_modules/gulp-workflow/.jscsrc'
    }
  },
  javascript: {
    all: 'components/**/*.js',
    src: 'components/*.js',
    dest: 'dist/js'
  },
  modernizr: {
    settings: {
      'cache' : true,
      'options' : [
        'setClasses',
          'html5printshiv'
      ]
    }
  },
  preDom: {
    src: [
      'dist/js/modernizr.js',
      'node_modules/picturefill/dist/picturefill.js',
      'node_modules/lazysizes/lazysizes.js'
    ],
    file: 'predom.js',
    dest: 'dist/js'
  },
  sass: {
    lintConfig: {
      settings: {
        options: {
          'config-file': 'node_modules/gulp-workflow/.sass-lint.yml'
        }
      }
    },
    options: {
      development: {
        outputStyle : 'nested',
          precision: 10,
          sourceMap: true
      },
      production: {
        outputStyle : 'compressed',
          precision: 10,
          sourceMap: false
      }
    },
    src: 'components/**/*.scss'
  },
  templates: {
    src: '../templates/*.html'
  }
}
