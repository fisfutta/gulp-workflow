'use strict';

module.exports = function (gulp, gulpconfigLocal) {

  // Get default configuration
  var gulpconfig = require('./gulpconfig');
  var jshintConfig  = require('./jshintrc.json');
  var extend = require('extend');

  // Merge the default configuration with the local one
  if (gulpconfigLocal && typeof gulpconfigLocal === 'object') {
    gulpconfig = extend(true, gulpconfig, gulpconfigLocal)
  }

  /**
   * Build Modules (ordered alphabetically)
   */
  var autoprefixer = require('gulp-autoprefixer');
  //var browserify = require('gulp-browserify');
  var browserify = require('browserify');
  var streamify = require('gulp-streamify')
  var browserSync = require('browser-sync');
  var bust = require('gulp-buster');
  var del = require('del');
  var concat = require('gulp-concat');
  var gulpif = require('gulp-if');
  var imagemin = require('gulp-imagemin');
  var jscs = require('gulp-jscs');
  var jshint = require('gulp-jshint');
  var modernizr = require('gulp-modernizr');
  var newer = require('gulp-newer');
  var reload = browserSync.reload;
  var runSequence = require('run-sequence').use(gulp);
  var sass = require('gulp-sass');
  var sassLint = require('gulp-sass-lint');
  var sourcemaps = require('gulp-sourcemaps');
  var uglify = require('gulp-uglify');
  var source = require('vinyl-source-stream');

  // Check if a --sourcemaps option is passed
  var useSourcemaps = process.argv.indexOf('--sourcemaps') !== -1;

  /**
   * All the Gulp tasks (ordered alphabetically)
   *
   * If you change things: please put all the configuration into gulpconfig.js.
   * This makes sure that things can be reused, support readability and
   * provide the possibility that they can be adjusted (only if necessary) in
   * the projects via the gulpconfig.local.js
   */

  // Clean up dist folders to remove outdated files
  gulp.task('clean:production', function () {
    return del([gulpconfig.clean.path]);
  });

  // Copy assets to the dist folder
  gulp.task('copy:js:libs:development', function () {
    return gulp.src(gulpconfig.copy.js.libs.src)
      .pipe(gulp.dest(gulpconfig.copy.js.libs.dest));
  });

  gulp.task('copy:js:libs:production', function () {
    return gulp.src(gulpconfig.copy.js.libs.src)
      .pipe(uglify())
      .pipe(gulp.dest(gulpconfig.copy.js.libs.dest));
  });

  gulp.task('copy:fonts:development', function () {
    return gulp.src(gulpconfig.copy.fonts.src)
      .pipe(gulp.dest(gulpconfig.copy.fonts.dest));
  });

  gulp.task('copy:fonts:production', function () {
    return gulp.src(gulpconfig.copy.fonts.src)
      .pipe(gulp.dest(gulpconfig.copy.fonts.dest));
  });

  gulp.task('copy:webfonts:development', function () {
    return gulp.src(gulpconfig.copy.webfonts.src)
      .pipe(gulp.dest(gulpconfig.copy.webfonts.dest));
  });

  gulp.task('copy:webfonts:production', function () {
    return gulp.src(gulpconfig.copy.webfonts.src)
      .pipe(gulp.dest(gulpconfig.copy.webfonts.dest));
  });

  // Losslessly optimize only new images and move them to the dist folder
  gulp.task('imagemin:development', function () {
    return gulp.src(gulpconfig.imagemin.src)
      .pipe(newer(gulpconfig.imagemin.dest))
      .pipe(imagemin(gulpconfig.imagemin.options))
      .pipe(gulp.dest(gulpconfig.imagemin.dest));
  });

  // Losslessly optimize all images and move them to the dist folder
  gulp.task('imagemin:production', function () {
    return gulp.src(gulpconfig.imagemin.src)
      .pipe(imagemin(gulpconfig.imagemin.options))
      .pipe(gulp.dest(gulpconfig.imagemin.dest));
  });

  // JavaScript Code Style Checker
  gulp.task('jscs:development', function () {
    return gulp.src(gulpconfig.javascript.all)
      .pipe(jscs(gulpconfig.jscs.options))
      .pipe(jscs.reporter());
  });



  gulp.task('javascript:development', function() {
    return browserify('./components/main.js')
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('main.js'))
        // Start piping stream to tasks!
        .pipe(gulp.dest(gulpconfig.javascript.dest))
});


  gulp.task('javascript:production', function() {
    return browserify('./components/main.js')
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        // Start piping stream to tasks!
        .pipe(source('main.js'))
        .pipe(streamify(uglify({mangle: false})))
        .pipe(gulp.dest(gulpconfig.javascript.dest))
        .pipe(streamify(bust(gulpconfig.bust.options)))
        .pipe(gulp.dest(gulpconfig.bust.dest));
});

  // Merge javascripts that need to run before the DOM is ready
  gulp.task('javascript-predom:development', ['modernizr'], function () {
    return gulp.src(gulpconfig.preDom.src)
      .pipe(concat(gulpconfig.preDom.file))
      .pipe(gulp.dest(gulpconfig.preDom.dest));
  });

  gulp.task('javascript-predom:production', ['modernizr'], function () {
    return gulp.src(gulpconfig.preDom.src)
      .pipe(uglify())
      .pipe(concat(gulpconfig.preDom.file))
      .pipe(gulp.dest(gulpconfig.preDom.dest))
      .pipe(bust(gulpconfig.bust.options))
      .pipe(gulp.dest(gulpconfig.bust.dest));
  });

  // Create a task that ensures the `js` task is complete before
  // reloading browsers via BrowserSync
  gulp.task('javascript-watch', ['javascript:development'], browserSync.reload);

  // Enforce JavaScript code style and prevent errors
  gulp.task('jshint:development', function() {
    return gulp.src(gulpconfig.javascript.all)
      .pipe(jshint(jshintConfig))
      .pipe(jshint.reporter('jshint-stylish'));
  });

  // Make a Custom Build of Modernizr
  var modernizrFiles = [];
  modernizrFiles = modernizrFiles.concat(gulpconfig.javascript.all, gulpconfig.sass.src);

  // The output will be concatenated by the `javascript-predom` task
  gulp.task('modernizr', function () {
    return gulp.src(modernizrFiles)
      .pipe(modernizr(gulpconfig.modernizr.settings))
      .pipe(gulp.dest(gulpconfig.javascript.dest));
  });

  // Compile Sass
  gulp.task('sass:development', function () {
    return gulp.src(gulpconfig.sass.src)
      .pipe(gulpif(useSourcemaps, sourcemaps.init())) // Only with --sourcemaps flag
      .pipe(sass(gulpconfig.sass.options.development).on('error', sass.logError))
      .pipe(autoprefixer(gulpconfig.autoprefixer))
      .pipe(gulpif(useSourcemaps, sourcemaps.write())) // Only with --sourcemaps flag
      .pipe(gulp.dest(gulpconfig.css.dest))
      .pipe(browserSync.stream());
  });

  gulp.task('sass:production', function () {
    return gulp.src(gulpconfig.sass.src)
      .pipe(sass(gulpconfig.sass.options.production))
      .pipe(autoprefixer(gulpconfig.autoprefixer))
      .pipe(gulp.dest(gulpconfig.css.dest))
      .pipe(bust(gulpconfig.bust.options))
      .pipe(gulp.dest(gulpconfig.bust.dest));
  });

  // Code style check for Sass
  gulp.task('sasslint:development', function () {
    return gulp.src(gulpconfig.sass.src)
      .pipe(sassLint(gulpconfig.sass.lintConfig.settings))
      .pipe(sassLint.format());
  });

  /**
   * Build Tasks for development and production
   */
  gulp.task('default', ['build:development']);

  // Static Server + watching scss/html files
  gulp.task('serve', ['build:development'], function() {

      // Don’t open the browser window automativally, if --no-open argument is passed
      if (process.argv.indexOf('--no-open') !== -1) {
        gulpconfig.browsersync.options.open = false;
      }

      browserSync.init(gulpconfig.browsersync.options);

      gulp.watch(gulpconfig.sass.src, ['sass:development', 'sasslint:development']);
      gulp.watch(gulpconfig.imagemin.src, ['imagemin:development']);
      gulp.watch(gulpconfig.javascript.all, ['javascript-watch', 'jshint:development', 'jscs:development']);
      gulp.watch(gulpconfig.templates.src).on('change', browserSync.reload);
  });

  // Watching Sass and JavaScript files
  gulp.task('watch', ['build:development'], function() {
      gulp.watch(gulpconfig.sass.src, ['sass:development', 'sasslint:development']);
      gulp.watch(gulpconfig.javascript.all, ['javascript-watch', 'jshint:development', 'jscs:development']);
      gulp.watch(gulpconfig.imagemin.src, ['imagemin:development']);
  });

  // Build files for development (uncompressed)
  gulp.task('build:development', [
    'copy:js:libs:development',
    'copy:fonts:development',
    'copy:webfonts:development',
    'imagemin:development',
    'javascript:development',
    'javascript-predom:development',
    'jscs:development',
    'jshint:development',
    'sass:development',
    'sasslint:development'
  ]);

  // Build files for production (compression, fail on errors)
  gulp.task('build:production', function (callback) {
    runSequence('clean:production', [
      'copy:js:libs:production',
      'copy:fonts:production',
      'copy:webfonts:production',
      'imagemin:production',
      'javascript:production',
      'javascript-predom:production',
      'sass:production'
    ], callback);
  });
};
